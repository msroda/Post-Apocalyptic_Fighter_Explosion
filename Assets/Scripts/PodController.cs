﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PodController : MonoBehaviour {

    private Transform m_Target;
    private Transform m_Gun;
    private GameObject m_Beam;
	private GameObject m_WarningLight;

    private bool m_Direction;

    [SerializeField]
    private float m_Speed;

    [SerializeField]
    private float m_ShootCooldown;
    private float m_ShootCooldownTimer;
    private bool m_CanShoot;

    private bool m_CanPowerShoot;

    [SerializeField]
    private float m_BulletSpeed;

    [SerializeField]
    private float m_BulletLifetime;

    [SerializeField]
    private GameObject m_Ammo;

    [SerializeField]
    private float m_ChargingTime;
    private float m_ChargingTimer;
    private bool m_IsCharging;

    [SerializeField]
    private float m_FiringTime;
    private float m_FiringTimer;
    private bool m_IsFiring;

    private bool m_ShouldFlip;

    public float m_MaxEnergy;
    public float m_Energy;

    [SerializeField]
    private float m_ShootCost;

    [SerializeField]
    private float m_PowerShootCost;

    [SerializeField]
    private float m_RechargeRate;

    [SerializeField]
    private float m_RechargeCooldown;
    private float m_RechargeTimer;

    private void Awake()
    {
        m_Direction = true;
        m_CanShoot = true;
        m_ShootCooldownTimer = 0.0f;

        m_CanPowerShoot = true;

        m_IsCharging = false;
        m_ChargingTimer = 0.0f;

        m_IsFiring = false;
        m_FiringTimer = 0.0f;

        m_Gun = transform.Find("Gun");
        m_Beam = transform.Find("Beam").gameObject;
		m_WarningLight = transform.Find("WarningLight").gameObject;

		m_WarningLight.SetActive (false);
        m_Beam.SetActive(false);

        m_ShouldFlip = false;

        m_Energy = m_MaxEnergy;

        m_RechargeTimer = 0.0f;
    }

	void Update ()
    {
        HandleTimers();

        if (!m_IsCharging && !m_IsFiring)
            Follow();         
	}



    public void Flip()
    {
        if (!m_IsCharging && !m_IsFiring)
        {
            m_Direction = !m_Direction;

            Vector3 theScale = transform.localScale;
            theScale.x *= -1;
            transform.localScale = theScale;
            m_ShouldFlip = false;
        }
        else
            m_ShouldFlip = !m_ShouldFlip;
    }

    public void Shoot()
    {
        if(m_CanShoot && !m_IsCharging && !m_IsFiring && m_Energy >= m_ShootCost)
        {
            m_Energy -= m_ShootCost;

            m_RechargeTimer = m_RechargeCooldown;

            m_CanShoot = false;

            GameObject newBullet = Instantiate(m_Ammo);

            newBullet.transform.position = m_Gun.position;

            if (m_Direction)
                newBullet.GetComponent<Rigidbody2D>().AddForce(new Vector2(m_BulletSpeed, 0.0f));
            else
                newBullet.GetComponent<Rigidbody2D>().AddForce(new Vector2(-m_BulletSpeed, 0.0f));

            Destroy(newBullet, m_BulletLifetime);
        }

    }

    public void PowerShoot()
    {
        if(!m_IsCharging && !m_IsFiring && m_Energy >= m_PowerShootCost)
        {
            m_Energy -= m_PowerShootCost;

            m_RechargeTimer = m_RechargeCooldown;

            m_IsCharging = true;
            m_WarningLight.SetActive(true);
        }
    }

    public void SetTargetToFollow(Transform target)
    {
        m_Target = target;
    }

    private void HandleTimers()
    {
        if (!m_CanShoot)
        {
            m_ShootCooldownTimer += Time.deltaTime;
            if (m_ShootCooldownTimer >= m_ShootCooldown)
            {
                m_CanShoot = true;
                m_ShootCooldownTimer = 0.0f;
            }
        }

        if (m_IsCharging)
        {
            m_ChargingTimer += Time.deltaTime;
            if (m_ChargingTimer >= m_ChargingTime)
            {
                m_IsCharging = false;
                m_IsFiring = true;
                m_ChargingTimer = 0.0f;
                m_Beam.SetActive(true);
				m_WarningLight.SetActive(false);
            }
        }

        if (m_IsFiring)
        {
            m_FiringTimer += Time.deltaTime;
            if (m_FiringTimer >= m_FiringTime)
            {
                m_IsFiring = false;
                m_FiringTimer = 0.0f;
                m_Beam.SetActive(false);
            }
        }

        if (!m_IsFiring && !m_IsCharging && m_ShouldFlip)
        {
            Flip();
        }

        if (m_RechargeTimer > 0.0f)
        {
            m_RechargeTimer = Mathf.Max(m_RechargeTimer - Time.deltaTime, 0.0f);
        }
        else
        {
            m_Energy = Mathf.Min(m_Energy + m_RechargeRate * Time.deltaTime, m_MaxEnergy);
        }
    }

    private void Follow()
    {
        if (m_Target)
        {
            Vector2 distance = m_Target.position - this.transform.position;
            Vector2 step = distance.normalized * m_Speed * Time.deltaTime;

            if (Mathf.Abs(step.magnitude) >= Mathf.Abs(distance.magnitude))
            {
                this.transform.position = m_Target.position;
            }
            else
                this.transform.position += new Vector3(step.x, step.y, 0.0f);
        }
    }

    public bool IsBusy()
    {
        return m_IsCharging || m_IsFiring;
    }
}
