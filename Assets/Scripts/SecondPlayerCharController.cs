﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class SecondPlayerCharController : MonoBehaviour {

	private CharController m_Character;
	private bool m_Jump;
	private bool m_Dash;
	private bool m_Float;
	private bool m_Shoot;
	private bool m_Attack;
	private bool m_Attack2;
	private bool m_PowerShoot;

    [SerializeField]
    private UIController m_UIController;

	[SerializeField]
	private float m_HoldLength;
	private float m_HoldTimer;

	[SerializeField]
	private PodController m_Pod;

	void Awake ()
	{
		m_Character = GetComponent<CharController>();
		m_HoldTimer = 0.0f;
	}

	private void Update()
	{
        if (m_UIController.m_GameOn == false)
            return;

        HandleTimers();
		if (!m_Jump)
		{
			m_Jump = CrossPlatformInputManager.GetButtonDown("Jump2");
		}

		m_Dash = (CrossPlatformInputManager.GetAxis ("Triggers2") < -0.75f);

		if (!m_Attack)
		{
			m_Attack = CrossPlatformInputManager.GetButtonDown("LightAttack2");
		}

		if (!m_Attack2)
		{
			m_Attack2 = CrossPlatformInputManager.GetButtonDown("HeavyAttack2");
		}

		m_Float = CrossPlatformInputManager.GetButton("Jump2");

		m_Shoot = (CrossPlatformInputManager.GetAxis ("Triggers2") > 0.75f);

		m_PowerShoot = CrossPlatformInputManager.GetButton ("PowerShoot2");

	}

	private void FixedUpdate()
	{
        if (m_UIController.m_GameOn == false)
            return;

        bool crouch = (CrossPlatformInputManager.GetAxis("Vertical2") < -0.5f);
		float h = CrossPlatformInputManager.GetAxis("Horizontal2");

		m_Character.Move(h, crouch, m_Jump, m_Dash, m_Float && (m_HoldTimer > m_HoldLength), m_Attack, m_Attack2);
		m_Jump = false;
		m_Dash = false;
		m_Attack = false;
		m_Attack2 = false;

		if(m_Shoot)
		{
			m_Pod.Shoot();
		}

		if (m_PowerShoot)
		{
			m_Pod.PowerShoot();
		}
	}

	private void HandleTimers()
	{
		if(m_Float)
		{
			m_HoldTimer += Time.deltaTime;
		}
		else
		{
			m_HoldTimer = 0.0f;
		}

	}
}
