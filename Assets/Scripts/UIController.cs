﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIController : MonoBehaviour {

    enum GameState
    {
        pregame,
        countdown,
        game,
        endgame
    }

    private GameState m_GameState;

    public bool m_GameOn;

    [SerializeField]
    private Text m_TextField;

    [SerializeField]
    private Slider m_HealthBarLeft;

    [SerializeField]
    private Slider m_HealthBarRight;

    [SerializeField]
    private Slider m_EnergyBarLeft;

    [SerializeField]
    private Slider m_EnergyBarRight;

    [SerializeField]
    private CharController m_LeftPlayer;

    [SerializeField]
    private PodController m_LeftPod;

    [SerializeField]
    private CharController m_RightPlayer;

    [SerializeField]
    private PodController m_RightPod;

    private float m_RightHPTarget;
    private float m_LeftHPTarget;

    private float m_RightEnergyTarget;
    private float m_LeftEnergyTarget;

    [SerializeField]
    private float m_AnimationSpeed;

    [SerializeField]
    private float m_LastHitWait;
    private float m_LastHitTimer;

    [SerializeField]
    private float m_EndMessageWait;
    private float m_EndMessageTimer;

    private float m_CountDownTimer;

    private int m_CountdownNumber;

	void Start ()
    {
        Time.timeScale = 1.1f;

        m_HealthBarLeft.value = 1.0f;
        m_HealthBarRight.value = 1.0f;

        m_EnergyBarLeft.value = 1.0f;
        m_EnergyBarRight.value = 1.0f;

        m_RightHPTarget = 1.0f;
        m_LeftHPTarget = 1.0f;

        m_RightEnergyTarget = 1.0f;
        m_LeftEnergyTarget = 1.0f;

        m_GameOn = false;

        m_LastHitTimer = 0.0f;

        m_EndMessageTimer = m_EndMessageWait;
        m_LastHitTimer = m_LastHitWait;

        m_CountdownNumber = 3;
        m_CountDownTimer = 1.0f;

        m_GameState = GameState.pregame;

        Cursor.visible = false;
	}
	
	void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            Application.Quit();

        if (m_GameState == GameState.pregame)
        {
            if (Input.GetKeyDown(KeyCode.Return))
            {
                m_GameState = GameState.countdown;
                m_TextField.text = m_CountdownNumber.ToString();
            }
        }

        HandleTimers();
        HandleBars();
    }

    private void HandleBars()
    {
        m_RightHPTarget = m_RightPlayer.m_HP / m_RightPlayer.m_MaxHP;
        m_LeftHPTarget = m_LeftPlayer.m_HP / m_LeftPlayer.m_MaxHP;

        m_RightEnergyTarget = m_RightPod.m_Energy / m_RightPod.m_MaxEnergy;
        m_LeftEnergyTarget = m_LeftPod.m_Energy / m_LeftPod.m_MaxEnergy;

        if (m_HealthBarRight.value < m_RightHPTarget)
        {
            m_HealthBarRight.value = Mathf.Min(m_RightHPTarget, m_HealthBarRight.value + m_AnimationSpeed * Time.deltaTime);
        }

        if (m_HealthBarRight.value > m_RightHPTarget)
        {
            m_HealthBarRight.value = Mathf.Max(m_RightHPTarget, m_HealthBarRight.value - m_AnimationSpeed * Time.deltaTime);
        }

        if (m_HealthBarLeft.value < m_LeftHPTarget)
        {
            m_HealthBarLeft.value = Mathf.Min(m_LeftHPTarget, m_HealthBarLeft.value + m_AnimationSpeed * Time.deltaTime);
        }

        if (m_HealthBarLeft.value > m_LeftHPTarget)
        {
            m_HealthBarLeft.value = Mathf.Max(m_LeftHPTarget, m_HealthBarLeft.value - m_AnimationSpeed * Time.deltaTime);
        }


        if (m_EnergyBarRight.value < m_RightEnergyTarget)
        {
            m_EnergyBarRight.value = Mathf.Min(m_RightEnergyTarget, m_EnergyBarRight.value + m_AnimationSpeed * Time.deltaTime);
        }

        if (m_EnergyBarRight.value > m_RightEnergyTarget)
        {
            m_EnergyBarRight.value = Mathf.Max(m_RightEnergyTarget, m_EnergyBarRight.value - m_AnimationSpeed * Time.deltaTime);
        }

        if (m_EnergyBarLeft.value < m_LeftEnergyTarget)
        {
            m_EnergyBarLeft.value = Mathf.Min(m_LeftEnergyTarget, m_EnergyBarLeft.value + m_AnimationSpeed * Time.deltaTime);
        }

        if (m_EnergyBarLeft.value > m_LeftEnergyTarget)
        {
            m_EnergyBarLeft.value = Mathf.Max(m_LeftEnergyTarget, m_EnergyBarLeft.value - m_AnimationSpeed * Time.deltaTime);
        }

        if (m_LeftHPTarget <= 0.0f || m_RightHPTarget <= 0.0f)
        {
            m_GameOn = false;
            m_GameState = GameState.endgame;
        }
            
    }

    void HandleTimers()
    {
        if (m_GameState == GameState.countdown)
        {
            if (m_CountDownTimer > 0.0f)
            {
                m_CountDownTimer -= Time.deltaTime;
            }
            else
            {
                m_CountDownTimer = 1.0f;
                m_CountdownNumber--;
                if(m_CountdownNumber == 0)
                    m_TextField.text = "GO";
                else if(m_CountdownNumber > 0)
                    m_TextField.text = m_CountdownNumber.ToString();
                else
                {
                    m_TextField.gameObject.SetActive(false);
                    m_GameState = GameState.game;
                    m_GameOn = true;
                }
            }
        }
        else if (m_GameState == GameState.endgame)
        {
            if (m_LastHitTimer > 0.0f)
            {
                m_LastHitTimer -= Time.deltaTime;
            }
            else
            {
                if (m_LeftHPTarget <= 0.0f)
                {
                    if (m_RightHPTarget <= 0.0f)
                    {
                        m_TextField.text = "Draw!";
                    }
                    else
                    {
                        m_TextField.text = "Player 2 wins!";
                    }
                }
                else
                {
                    m_TextField.text = "Player 1 wins!";
                }

                m_TextField.gameObject.SetActive(true);
            }

            if (m_EndMessageTimer > 0.0f)
            {
                m_EndMessageTimer -= Time.deltaTime;
            }
            else
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            }

        }
    }
}
