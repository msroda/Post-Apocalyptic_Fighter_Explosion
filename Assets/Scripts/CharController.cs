﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharController : MonoBehaviour {

    [SerializeField]
    private bool m_Flip;

	public float m_MaxHP;
	public float m_HP;

    [SerializeField]
    private GameObject m_Ground;

    [SerializeField]
    private float m_HorizontalSpeed;

    [SerializeField]
    private float m_AirHorizontalSpeed;

    [SerializeField]
    private float m_DashSpeed;

	[SerializeField]
	private float m_DashDuration;
	private float m_DashTimer;
	private bool m_IsDashing;

    [SerializeField]
    private float m_Acceleration;

    [SerializeField]
    private float m_Deceleration;

	[SerializeField]
	private float m_SlideDeceleration;

    [SerializeField]
    private float m_JumpForce;

    [SerializeField]
    private float m_FloatSpeed;

    private bool m_Inert;
	private float m_InertTimer;

    private Transform m_GroundCheck;
    private Animator m_Animator;
    private Rigidbody2D m_Rigidbody;

    private bool m_Direction;
    private bool m_Grounded;

    private bool m_CanDoubleJump;

    [SerializeField]
    private float m_DashCooldown;
    private float m_DashCooldownTimer;
    private bool m_CanDash;

	[SerializeField]
	private float m_AttackCooldown;
	private float m_AttackCooldownTimer;
	private bool m_CanAttack;

	private float m_OriginalGravity;

    [SerializeField]
    private Vector2 m_CrouchColliderSize;
    [SerializeField]
    private Vector2 m_CrouchColliderOffset;

    private CapsuleCollider2D m_Collider;

    private Vector2 m_OriginalColliderSize;
    private Vector2 m_OriginalColliderOffset;

    private Transform m_NormalPod;
    private Transform m_CrouchingPod;
    private Transform m_FloatingPod;

	private bool m_IsAttacking;

    [SerializeField]
    private PodController m_Pod;

	private GameObject m_Attack1;
	private GameObject m_Attack2;
	private GameObject m_Attack3;
	private GameObject m_AirAttack;
	private GameObject m_HeavyAttack;
	private GameObject m_CrouchedAttack;

    void Awake ()
    {
        m_GroundCheck = transform.Find("GroundCheck");
        m_Animator = transform.Find("Animator").gameObject.GetComponent<Animator>();
        m_Rigidbody = GetComponent<Rigidbody2D>();
        m_Collider = GetComponent<CapsuleCollider2D>();

		m_Attack1 = transform.Find("Attack1").gameObject;
		m_Attack2 = transform.Find("Attack2").gameObject;
		m_Attack3 = transform.Find("Attack3").gameObject;
		m_AirAttack = transform.Find("AirAttack").gameObject;
		m_HeavyAttack = transform.Find("HeavyAttack").gameObject;
		m_CrouchedAttack = transform.Find("CrouchedAttack").gameObject;

        m_NormalPod = transform.Find("NormalPod");
        m_CrouchingPod = transform.Find("CrouchingPod");
        m_FloatingPod = transform.Find("FloatingPod");

        m_Direction = true;

        m_Inert = false;
        m_Animator.SetBool("Inert", m_Inert);

        m_CanDash = true;
        m_DashCooldownTimer = 0.0f;

		m_OriginalGravity = m_Rigidbody.gravityScale;

        m_OriginalColliderSize = m_Collider.size;
        m_OriginalColliderOffset = m_Collider.offset;

		m_IsAttacking = false;

		m_CanAttack = true;
		m_AttackCooldownTimer = 0.0f;

		m_HP = m_MaxHP;
    }

    void Start()
    {
        if (m_Flip)
        {
            Flip();
        }
    }

    void FixedUpdate()
    {
        m_Grounded = m_Ground.GetComponent<Collider2D>().OverlapPoint(m_GroundCheck.transform.position);

		if(m_Grounded)
		{
			m_Inert = false;
			m_InertTimer = 0.0f;
		}			

        m_Animator.SetBool("Grounded", m_Grounded);
    }

	void Update()
	{
		HandleTimers();
	}

	public void Move(float move, bool crouch, bool jump, bool dash, bool floating, bool attack, bool attack2)
    {
        string tempClipName = m_Animator.GetCurrentAnimatorClipInfo(0)[0].clip.name;
		
        if(!m_Inert && (tempClipName != "Hurt"))
		{

			m_Animator.SetBool("Crouch", crouch);

			m_Animator.SetBool("DoubleJump", false);

			if (attack && m_CanAttack)
			{
				m_Animator.SetTrigger("Attack");
				m_IsAttacking = true;
			}

			if (attack2 && m_CanAttack && m_Grounded)
			{
				m_Animator.SetTrigger("HeavyAttack");
				m_IsAttacking = true;
			}

			float deceleration;

			if (crouch)
			{
				move = 0;
				deceleration = m_SlideDeceleration;
			}
			else if (!m_Grounded && !m_IsAttacking)
				deceleration = m_SlideDeceleration;
			else
				deceleration = m_Deceleration;



			float speed;
			float vSpeed = m_Rigidbody.velocity.y;

			if (m_IsAttacking)
			{
				move = 0.0f;
				vSpeed = 0.0f;
			}

			if (dash && m_CanDash && !crouch)
			{
				m_Animator.SetTrigger("Dash");

				m_CanDash = false;

				m_IsDashing = true;

				vSpeed = 0.0f;

				if (move == 0.0f)
				if (m_Direction)
					speed = m_DashSpeed;
				else
					speed = -m_DashSpeed;
				else
					if (move > 0.0f)
						speed = m_DashSpeed;
					else
						speed = -m_DashSpeed;
			}

			else
			{
				float targetSpeed;

				if (m_Grounded)
					targetSpeed = m_HorizontalSpeed;
				else
					targetSpeed = m_AirHorizontalSpeed;

				float currentVelocity = m_Rigidbody.velocity.x;

				if (move == 0.0f || crouch)
				{
					if (currentVelocity < 0.0f)
					{
						speed = currentVelocity + deceleration * Time.fixedDeltaTime;
						if (speed > 0.0f)
							speed = 0.0f;
					}
					else
					{
						speed = currentVelocity - deceleration * Time.fixedDeltaTime;
						if (speed < 0.0f)
							speed = 0.0f;
					}
				}
				else if (move > 0.0f)
				{
					if (currentVelocity < targetSpeed)
					{
						speed = currentVelocity + m_Acceleration * Time.fixedDeltaTime;
						if (speed > targetSpeed)
							speed = targetSpeed;
					}
					else
					{
						speed = currentVelocity - deceleration * Time.fixedDeltaTime;
						if (speed < targetSpeed)
							speed = targetSpeed;
					}

				}
				else
				{
					if (currentVelocity > -targetSpeed)
					{
						speed = currentVelocity - m_Acceleration * Time.fixedDeltaTime;
						if (speed < -targetSpeed)
							speed = -targetSpeed;
					}
					else
					{
						speed = currentVelocity + deceleration * Time.fixedDeltaTime;
						if (speed > -targetSpeed)
							speed = -targetSpeed;
					}

				}
			}

			m_Animator.SetFloat("Speed", Mathf.Abs(speed));

			m_Rigidbody.velocity = new Vector2(speed, vSpeed);

			if(!m_IsAttacking)
				if (move > 0 && !m_Direction)
				{
					Flip();
				}
				else if (move < 0 && m_Direction)
				{
					Flip();
				}

			if(m_Grounded)
			{
				if(jump)
				{
					m_Grounded = false;
					m_Animator.SetBool("Grounded", false);
					m_Rigidbody.velocity = new Vector2(m_Rigidbody.velocity.x, m_JumpForce);
					m_CanDoubleJump = true;
					m_IsDashing = false;
					m_DashTimer = 0.0f;
				}

			}
			else
			{
				if(jump && m_CanDoubleJump)
				{
					m_Animator.SetBool("DoubleJump", true);
					m_Rigidbody.velocity = new Vector2(m_Rigidbody.velocity.x, m_JumpForce);
					m_CanDoubleJump = false;
					m_IsDashing = false;
					m_DashTimer = 0.0f;
				}
			}

			if (floating && (m_Rigidbody.velocity.y <= m_FloatSpeed) && !m_Pod.IsBusy())
			{
				m_Animator.SetBool("Floating", true);
				m_Rigidbody.gravityScale = 0.0f;
				m_Rigidbody.velocity = new Vector2 (m_Rigidbody.velocity.x, m_FloatSpeed);
			}
			else
			{
				m_Animator.SetBool("Floating", false);
				m_Rigidbody.gravityScale = m_OriginalGravity;
			}

			if(m_IsDashing || m_IsAttacking)
				m_Rigidbody.gravityScale = 0.0f;
			else
				m_Rigidbody.gravityScale = m_OriginalGravity;

			SetTransforms();
		}
    }

    private void Flip()
    {
        m_Direction = !m_Direction;

        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;

        m_Pod.Flip();
    }

    private void HandleTimers()
    {
        if(!m_CanDash)
        {
            m_DashCooldownTimer += Time.deltaTime;
            if (m_DashCooldownTimer >= m_DashCooldown)
            {
                m_CanDash = true;
                m_DashCooldownTimer = 0.0f;
            }
        }
		if(m_IsDashing)
		{
			m_DashTimer += Time.deltaTime;
			if (m_DashTimer >= m_DashDuration)
			{
				m_IsDashing = false;
				m_DashTimer = 0.0f;
			}
		}
		if(!m_CanAttack)
		{
			m_AttackCooldownTimer += Time.deltaTime;
			if (m_AttackCooldownTimer >= m_AttackCooldown)
			{
				m_CanAttack = true;
				m_AttackCooldownTimer = 0.0f;
			}
		}
		if(m_InertTimer > 0.0f)
		{
			m_InertTimer -= Time.deltaTime;
			if(m_InertTimer <= 0.0f)
			{
				m_Inert = false;
                m_Animator.SetBool("Inert", m_Inert);
				m_InertTimer = 0.0f;
			}
		}
    }

    private void SetTransforms()
    {
        string tempClipName = m_Animator.GetCurrentAnimatorClipInfo(0)[0].clip.name;
        if (tempClipName == "Crouched")
        {
            m_Pod.SetTargetToFollow(m_CrouchingPod);
            m_Collider.offset = m_CrouchColliderOffset;
            m_Collider.size = m_CrouchColliderSize;
        }
        else if (tempClipName == "Floating" || tempClipName == "AirToFloat")
        {
            m_Pod.SetTargetToFollow(m_FloatingPod);
            m_Collider.offset = m_OriginalColliderOffset;
            m_Collider.size = m_OriginalColliderSize;
        }
        else
        {
            m_Pod.SetTargetToFollow(m_NormalPod);
            m_Collider.offset = m_OriginalColliderOffset;
            m_Collider.size = m_OriginalColliderSize;
        }

		if (tempClipName == "InAirAttackToInAir" ||
		    tempClipName == "Attack1ToIdle" ||
		    tempClipName == "Attack2ToIdle" ||
			tempClipName == "Attack3ToIdle" ||
			tempClipName == "HeavyAttackToIdle" ||
			tempClipName == "CrouchedAttackToCrouched")
		{
			m_CanAttack = false;
			m_IsAttacking = false;
		}

		if (tempClipName == "Attack1")
		{
			m_Attack1.SetActive(true);
			m_Attack2.SetActive(false);
			m_Attack3.SetActive(false);
			m_AirAttack.SetActive(false);
			m_HeavyAttack.SetActive(false);
			m_CrouchedAttack.SetActive(false);
		}
		else if (tempClipName == "Attack2")
		{
			m_Attack1.SetActive(false);
			m_Attack2.SetActive(true);
			m_Attack3.SetActive(false);
			m_AirAttack.SetActive(false);
			m_HeavyAttack.SetActive(false);
			m_CrouchedAttack.SetActive(false);
		}
		else if (tempClipName == "Attack3")
		{
			m_Attack1.SetActive(false);
			m_Attack2.SetActive(false);
			m_Attack3.SetActive(true);
			m_AirAttack.SetActive(false);
			m_HeavyAttack.SetActive(false);
			m_CrouchedAttack.SetActive(false);
		}
		else if (tempClipName == "InAirAttack")
		{
			m_Attack1.SetActive(false);
			m_Attack2.SetActive(false);
			m_Attack3.SetActive(false);
			m_AirAttack.SetActive(true);
			m_HeavyAttack.SetActive(false);
			m_CrouchedAttack.SetActive(false);
		}
		else if (tempClipName == "CrouchedAttack")
		{
			m_Attack1.SetActive(false);
			m_Attack2.SetActive(false);
			m_Attack3.SetActive(false);
			m_AirAttack.SetActive(false);
			m_HeavyAttack.SetActive(false);
			m_CrouchedAttack.SetActive(true);
		}
		else if (tempClipName == "HeavyAttack")
		{
			m_Attack1.SetActive(false);
			m_Attack2.SetActive(false);
			m_Attack3.SetActive(false);
			m_AirAttack.SetActive(false);
			m_HeavyAttack.SetActive(true);
			m_CrouchedAttack.SetActive(false);
		}
		else
		{
			m_Attack1.SetActive(false);
			m_Attack2.SetActive(false);
			m_Attack3.SetActive(false);
			m_AirAttack.SetActive(false);
			m_HeavyAttack.SetActive(false);
			m_CrouchedAttack.SetActive(false);
		}

    }

	public void Hit(float damage, float knockUpForce, float sourcePos, float inertTime)
	{
		m_HP -= damage;

		m_Inert = true;
        m_Animator.SetBool("Inert", m_Inert);
        m_IsDashing = false;
        m_IsAttacking = false;
		m_InertTimer = Mathf.Max(m_InertTimer, inertTime);
		m_Animator.SetTrigger("Hit");
		m_Rigidbody.gravityScale = m_OriginalGravity;

		if (sourcePos < this.transform.position.x)
			m_Rigidbody.velocity = new Vector2 (knockUpForce/3, knockUpForce);
		else
			m_Rigidbody.velocity = new Vector2 (-knockUpForce/3, knockUpForce);
	}
}
