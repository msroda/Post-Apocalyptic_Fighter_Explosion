﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitController : MonoBehaviour {

    enum HitType
    {
        bullet,
        sword,
        beam
    }

    List<GameObject> m_GameObjects;

	[SerializeField]
	private bool m_DestroyOnCollision;

	[SerializeField]
	private Transform m_Source;

	[SerializeField]
	private float m_Damage;

	[SerializeField]
	private float m_KnockUpForce;

	[SerializeField]
	private float m_InertTime;

    [SerializeField]
    private HitType m_Type;

	private Collider2D m_Collider;
	
	void OnTriggerEnter2D(Collider2D col)
	{
		GameObject secondObject = col.gameObject;

        if (!m_GameObjects.Contains(secondObject))
        {

            CharController charController = secondObject.GetComponent<CharController>();
            BombController bombController = secondObject.GetComponent<BombController>();

            if (charController)
            {
                if (m_Source)
                    charController.Hit(m_Damage, m_KnockUpForce, m_Source.position.x, m_InertTime);
                else
                    charController.Hit(m_Damage, m_KnockUpForce, this.transform.position.x, m_InertTime);
                m_GameObjects.Add(secondObject);
            }

            if (bombController)
            {
                if (m_Type == HitType.beam)
                    bombController.Explode();
                else if (m_Type == HitType.sword)
                {
                    if (m_Source)
                        bombController.ChangeDir(m_Source.position.x);
                    else
                        bombController.ChangeDir(this.transform.position.x);
                }
                m_GameObjects.Add(secondObject);
            }
        } 

		if (m_DestroyOnCollision)
			Destroy(this.gameObject);
	}

    private void OnEnable()
    {
        m_GameObjects.Clear();
    }

    private void Awake()
    {
        m_GameObjects = new List<GameObject>();
        m_GameObjects.Clear();
    }
}


