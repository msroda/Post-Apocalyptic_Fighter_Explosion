﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombController : MonoBehaviour {

    [SerializeField]
    private float m_FreeFallingSpeed;
    [SerializeField]
    private float m_FastFallingSpeed;

    private GameObject m_Explosion;
    private Rigidbody2D m_Rigidbody;

    private Vector3 m_Velocity;

    private bool m_Exploded;

	void Start ()
    {
        m_Explosion = transform.Find("Explosion").gameObject;
        m_Velocity = new Vector3(0.0f, -m_FreeFallingSpeed, 0.0f);
        m_Exploded = false;
	}

    public void ChangeDir(float source)
    {
        if (!m_Exploded)
        {
            float horizontal;

            if (source < this.transform.position.x)
                horizontal = m_FastFallingSpeed;
            else
                horizontal = -m_FastFallingSpeed;

            m_Velocity = new Vector3(horizontal, -m_FreeFallingSpeed, 0.0f);
        }
    }

    public void Explode()
    {
        m_Explosion.SetActive(true);
        m_Velocity = Vector3.zero;
        m_Exploded = true;
    }

    void OnCollisionEnter2D(Collision2D Col)
    {
        if (Col.gameObject.tag == "Floor")
            this.Explode();
    }

    void FixedUpdate()
    {
        Move();
    }

    private void Move()
    {
        this.transform.position += m_Velocity * Time.fixedDeltaTime;
    }
}
