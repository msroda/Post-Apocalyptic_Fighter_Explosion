﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionController : MonoBehaviour {

	private float m_Timer;
	[SerializeField]
	private float m_Radius;
	[SerializeField]
	private float m_GrowthTime;

	private Animator m_Animator;

	private Collider2D m_Collider;

    private float m_ExplosionHeight;

	void Start ()
	{
		m_Timer = 0.0f;
		m_Collider = GetComponent<Collider2D>();
		m_Animator = GetComponent<Animator>();
        m_ExplosionHeight = this.transform.position.y;
	}

	void Update ()
	{
		if(m_Timer < m_GrowthTime)
		{
			m_Timer += Time.deltaTime;
		}

		float scale = Mathf.Min (1, m_Timer / m_GrowthTime) * m_Radius;

		this.transform.localScale = new Vector3(scale, scale, scale);
        this.transform.position = new Vector3(this.transform.position.x, m_ExplosionHeight + scale / 10, this.transform.position.z);

		string tempClipName = m_Animator.GetCurrentAnimatorClipInfo(0)[0].clip.name;

		if (tempClipName == "Disappear")
		{
            m_Collider.enabled = false;
            this.transform.parent.gameObject.GetComponent<SpriteRenderer>().enabled = false;
		}
		else if (tempClipName == "Gone")
		{
            Destroy(this.transform.parent.gameObject);
		}
	}
}
