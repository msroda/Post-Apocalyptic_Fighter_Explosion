﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerController : MonoBehaviour {

    [SerializeField]
    private GameObject m_Bomb;

    [SerializeField]
    private GameObject m_LeftWall;

    [SerializeField]
    private GameObject m_RightWall;

    [SerializeField]
    private float m_Margin;

    [SerializeField]
    private float m_MinInterval;

    [SerializeField]
    private float m_MaxInterval;

    [SerializeField]
    private UIController m_UIController;

    private float m_Timer;

    void Start ()
    {
        m_Timer = Random.value * (m_MaxInterval - m_MinInterval) + m_MinInterval;
	}
	
	void Update ()
    {
        if (m_UIController.m_GameOn == false)
            return;

        m_Timer -= Time.deltaTime;

        if (m_Timer <= 0.0f)
        {
            GameObject newBomb = Instantiate(m_Bomb);
            newBomb.transform.position = new Vector3(Random.value * (m_RightWall.transform.position.x - m_LeftWall.transform.position.x - 2 * m_Margin) + m_LeftWall.transform.position.x + m_Margin, this.transform.position.y, newBomb.transform.position.z);

            m_Timer = Random.value * (m_MaxInterval - m_MinInterval) + m_MinInterval;
        }
	}
}
